unit UntReadThread;

interface

uses
  System.Classes,
  IdTCPConnection, IdTCPClient, IdBaseComponent, IdComponent, IdCustomTCPServer,
  IdTCPServer, IdContext;

type
  TDataEvent = procedure(const Data: AnsiString) of object;

  TReadingThread = class(TThread)
  private
    FClient: TIdTCPClient;
    FData: string;
    FOnData: TDataEvent;
    procedure DataReceived;
  protected
    procedure Execute; override;
  public
    constructor Create(AClient: TIdTCPClient); reintroduce;
    property OnData: TDataEvent read FOnData write FOnData;
  end;

implementation

constructor TReadingThread.Create(AClient: TIdTCPClient);
begin
  inherited Create(True);
  FreeOnTerminate := True;
  FClient := AClient;
end;

procedure TReadingThread.Execute;
begin
  while not Terminated do
  begin
    FData := FClient.IOHandler.ReadLn();
    if (FData <> '') and Assigned(FOnData) then
      Synchronize(DataReceived);
  end;
end;

procedure TReadingThread.DataReceived;
begin
  if Assigned(FOnData) then
    FOnData(FData);
end;

end.
