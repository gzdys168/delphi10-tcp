program Tcp;

uses
  Vcl.Forms,
  UntTcp in 'UntTcp.pas' {frmTcp},
  UntReadThread in 'UntReadThread.pas',
  DYSChar in 'DYSChar.pas',
  DYSNet in 'DYSNet.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmTcp, frmTcp);
  Application.Run;
end.
