unit UntTcp;

interface

uses
  UntReadThread,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls,
  IdTCPConnection, IdTCPClient, IdBaseComponent, IdComponent, IdCustomTCPServer,
  IdTCPServer, IdContext, Vcl.ComCtrls;

type
  TfrmTcp = class(TForm)
    pnlMain: TPanel;
    pnlLeft: TPanel;
    Label1: TLabel;
    cbServer: TCheckBox;
    edtPort: TEdit;
    Label2: TLabel;
    edtIP: TEdit;
    btnStart: TButton;
    IdTCPServer1: TIdTCPServer;
    IdTCPClient1: TIdTCPClient;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    pnlRight: TPanel;
    pnlTop: TPanel;
    GroupBox1: TGroupBox;
    mmIN: TMemo;
    pnlBottom: TPanel;
    Panel1: TPanel;
    btnSend: TButton;
    GroupBox2: TGroupBox;
    Edit1: TEdit;
    Label5: TLabel;
    edtName: TEdit;
    procedure cbServerClick(Sender: TObject);
    procedure btnStartClick(Sender: TObject);
    procedure IdTCPServer1Execute(AContext: TIdContext);
    procedure btnSendClick(Sender: TObject);
    procedure IdTCPServer1Connect(AContext: TIdContext);
    procedure FormCreate(Sender: TObject);
  private

  public
    clientThread: TReadingThread;
    procedure DataReceived(const Data: AnsiString);
  end;

var
  frmTcp: TfrmTcp;

implementation

{$R *.dfm}

uses DYSChar, DYSNet;

procedure TfrmTcp.btnSendClick(Sender: TObject); // 发送消息
var
  i: integer;
  Context: TIdContext;
  originStr: AnsiString;
  sendStr: AnsiString;
  toWho: AnsiString;
begin
  originStr := Trim(Edit1.Text);

  sendStr := '';
  for i := 1 to Length(originStr) do
  begin
    sendStr := sendStr + OneCToTwoC(originStr[i]);
  end;

  if cbServer.Checked and IdTCPServer1.Active then // 服务器
  begin
    with IdTCPServer1.Contexts.LockList do
      try
        for i := 0 to IdTCPServer1.Contexts.Count - 1 do
        begin
          Context := TIdContext(Items[i]);
          try
            Context.Connection.CheckForGracefulDisconnect(True);
            Context.Connection.IOHandler.WriteLn(sendStr);
            toWho := GetComputerNameByIP(Context.Binding.PeerIP);
            mmIN.Lines.Add('【' + toWho + '】：' + originStr);
          except
            Context.Connection.Disconnect;
          end;
        end;
      finally
        IdTCPServer1.Contexts.UnlockList;
      end;
  end

  else // 客服端
  begin
    if IdTCPClient1.Connected then
    begin
      IdTCPClient1.IOHandler.WriteLn(sendStr);
      toWho := GetComputerNameByIP(Trim(edtIP.Text));
      mmIN.Lines.Add('【' + toWho + '】：' + originStr);
    end
    else
      mmIN.Lines.Add('服务连接失败');
  end;
end;

// 开始TCP/IP通讯
procedure TfrmTcp.btnStartClick(Sender: TObject);
begin
  if cbServer.Checked then // 服务器
  begin
    if IdTCPServer1.Active then // 已启动
    begin
      Exit;
    end
    else // 没启动
    begin
      IdTCPServer1.DefaultPort := StrToIntDef(edtPort.Text, 45678);
      try
        IdTCPServer1.Active := True;

        cbServer.Enabled := False;
        edtPort.Enabled := False;
        edtIP.Enabled := False;

        mmIN.Lines.Add('启动服务成功');
      Except
        mmIN.Lines.Add('启动服务失败');
      end;
    end;
  end

  else // 客服端
  begin
    if IdTCPClient1.Connected then // 已启动
    begin
      Exit;
    end
    else // 没启动
    begin
      IdTCPClient1.Host := edtIP.Text;
      IdTCPClient1.Port := StrToIntDef(edtPort.Text, 45678);
      try
        IdTCPClient1.Connect;

        if IdTCPClient1.Connected then
        begin
          cbServer.Enabled := False;
          edtPort.Enabled := False;
          edtIP.Enabled := False;

          if not Assigned(clientThread) then
          begin
            clientThread := TReadingThread.Create(IdTCPClient1);
            clientThread.OnData := DataReceived;
            clientThread.Resume;
          end
          else if clientThread.Suspended then
          begin
            clientThread.Resume;
          end;
        end;

        mmIN.Lines.Add('连接服务器成功');
      Except
        mmIN.Lines.Add('连接服务器失败');
      end;
    end;
  end;
end;

// 类型选择
procedure TfrmTcp.cbServerClick(Sender: TObject);
begin
  if cbServer.Checked then
    btnStart.Caption := '启动服务'
  else
    btnStart.Caption := '连接服务';
end;

// 客服端读取信息
procedure TfrmTcp.DataReceived(const Data: AnsiString);
var
  i, iLen: integer;
  temp: AnsiString;
  fromWho: AnsiString;
begin
  iLen := Length(Data) div 2;
  temp := '';
  for i := 1 to iLen do
    temp := temp + TwoCtoOneC(Data[2 * i - 1], Data[2 * i]);

  fromWho := GetComputerNameByIP(Trim(edtIP.Text));
  mmIN.Lines.Add('【' + fromWho + '】：' + temp);
end;

// 窗体初始化
procedure TfrmTcp.FormCreate(Sender: TObject);
begin
  edtIP.Text := GetLocalIP();
  edtName.Text := GetNameByIPAddr(edtIP.Text);
end;

// 有客户端连接
procedure TfrmTcp.IdTCPServer1Connect(AContext: TIdContext);
begin
  mmIN.Lines.Add(AContext.Binding.PeerIP + ' ' +
    IntToStr(AContext.Binding.PeerPort) + ' 已连接');
end;

// 服务器接收
procedure TfrmTcp.IdTCPServer1Execute(AContext: TIdContext);
var
  i, iLen: integer;
  msg, temp: AnsiString;
  fromWho: AnsiString;
begin
  try
    AContext.Connection.CheckForGracefulDisconnect(True);
    msg := AContext.Connection.IOHandler.ReadLn();

    iLen := Length(msg) div 2;
    temp := '';
    for i := 1 to iLen do
      temp := temp + TwoCtoOneC(msg[2 * i - 1], msg[2 * i]);

    fromWho := GetComputerNameByIP(AContext.Binding.PeerIP);
    mmIN.Lines.Add('【' + fromWho + '】：' + temp);
  except
    AContext.Connection.Disconnect;
  end;
end;

end.
