object frmTcp: TfrmTcp
  Left = 0
  Top = 0
  Caption = 'TCP/IP'#20256#36755#27979#35797
  ClientHeight = 443
  ClientWidth = 744
  Color = clSkyBlue
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object pnlMain: TPanel
    Left = 0
    Top = 0
    Width = 744
    Height = 443
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlLeft: TPanel
      Left = 0
      Top = 0
      Width = 220
      Height = 443
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 6
        Top = 49
        Width = 60
        Height = 12
        Caption = #26381#21153#22120#31471#21475
      end
      object Label2: TLabel
        Left = 6
        Top = 77
        Width = 36
        Height = 12
        Caption = #26381#21153'IP'
      end
      object Label5: TLabel
        Left = 6
        Top = 105
        Width = 48
        Height = 12
        Caption = #26412#26426#21517#31216
      end
      object cbServer: TCheckBox
        Left = 72
        Top = 15
        Width = 97
        Height = 17
        Caption = #26381#21153#22120
        TabOrder = 0
        OnClick = cbServerClick
      end
      object edtPort: TEdit
        Left = 72
        Top = 45
        Width = 120
        Height = 20
        TabOrder = 1
        Text = '45678'
      end
      object edtIP: TEdit
        Left = 72
        Top = 73
        Width = 120
        Height = 20
        TabOrder = 2
        Text = '192.168.1.104'
      end
      object btnStart: TButton
        Left = 71
        Top = 129
        Width = 121
        Height = 25
        Caption = #36830#25509#26381#21153
        TabOrder = 3
        OnClick = btnStartClick
      end
      object edtName: TEdit
        Left = 72
        Top = 101
        Width = 120
        Height = 20
        Color = clInfoBk
        ReadOnly = True
        TabOrder = 4
      end
    end
    object PageControl1: TPageControl
      Left = 220
      Top = 0
      Width = 524
      Height = 443
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = #28040#24687#25910#21457
        object pnlRight: TPanel
          Left = 0
          Top = 0
          Width = 516
          Height = 415
          Align = alClient
          BevelOuter = bvNone
          Color = clSkyBlue
          ParentBackground = False
          TabOrder = 0
          object pnlTop: TPanel
            Left = 0
            Top = 0
            Width = 516
            Height = 340
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 0
              Top = 0
              Width = 516
              Height = 340
              Align = alClient
              Caption = #25509#25910#28040#24687
              TabOrder = 0
              object mmIN: TMemo
                Left = 2
                Top = 14
                Width = 512
                Height = 324
                Align = alClient
                Color = clInfoBk
                ReadOnly = True
                ScrollBars = ssBoth
                TabOrder = 0
              end
            end
          end
          object pnlBottom: TPanel
            Left = 0
            Top = 340
            Width = 516
            Height = 75
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel1: TPanel
              Left = 0
              Top = 45
              Width = 516
              Height = 30
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 0
              object btnSend: TButton
                Left = 439
                Top = 1
                Width = 75
                Height = 25
                Caption = #21457#36865#65288'&S'#65289
                TabOrder = 0
                OnClick = btnSendClick
              end
            end
            object GroupBox2: TGroupBox
              Left = 0
              Top = 0
              Width = 516
              Height = 45
              Align = alClient
              Caption = #21457#36865#28040#24687
              TabOrder = 1
              object Edit1: TEdit
                Left = 2
                Top = 14
                Width = 512
                Height = 29
                Align = alClient
                TabOrder = 0
                ExplicitHeight = 20
              end
            end
          end
        end
      end
    end
  end
  object IdTCPServer1: TIdTCPServer
    Bindings = <>
    DefaultPort = 0
    OnConnect = IdTCPServer1Connect
    OnExecute = IdTCPServer1Execute
    Left = 264
    Top = 56
  end
  object IdTCPClient1: TIdTCPClient
    ConnectTimeout = 1000
    IPVersion = Id_IPv4
    Port = 0
    ReadTimeout = -1
    Left = 344
    Top = 56
  end
end
